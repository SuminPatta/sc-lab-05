
public class main {
	
	public static void main(String[] args){
		Library lib = new Library();
		Student stu = new Student("mak","jacobsen","561040456"); //name,lastname,id
		Instructor instructor = new Instructor("usa","sammapan","d14");//name,lastname,id
		Staff stf = new Staff("jinhwan","kim");//name,lastname
		Book book1 = new Book("Big Java", 400,"Watson");//bookname,price,writer
		Book book2 = new Book("PPL", 500,"AJarn");//
		ReferencesBook refbook = new ReferencesBook("ABC",200,"john");//bookname,price,writer
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addBook(refbook);
		
		System.out.println(lib.bookCount());//3
		lib.borrowBook(stu,"Big Java");//borrow *mutator
		System.out.println(lib.bookCount());//2 *accessor
		System.out.println(stu.getBorrowedBook());//Big Java
		System.out.println(lib.getBorrowers());//mak jacobsen
		System.out.println(stu.getName());//mak jacobsen
		System.out.println(lib.bookCount());
		lib.returnBook(stu,"Big Java");//bookname *mutator
		System.out.println(lib.bookCount());//3
		System.out.println(lib.getBorrowers());//empty
		
		System.out.println(lib.bookCount());
		lib.borrowBook(stf,"Big Java");
		System.out.println(lib.bookCount());
		System.out.println(stf.getBorrowedBook());
		System.out.println(lib.getBorrowers());
		System.out.println(stf.getName());
		System.out.println(lib.bookCount());

		System.out.println(lib.bookCount());
		lib.borrowBook(instructor,"PPL");
		System.out.println(lib.bookCount());
		System.out.println(instructor.getBorrowedBook());
		System.out.println(lib.getBorrowers());
		System.out.println(instructor.getName());
		System.out.println(lib.bookCount());
		lib.returnBook(instructor,"PPL");
		System.out.println(lib.bookCount());
		System.out.println(lib.getBorrowers());
		lib.returnBook(stf,"Big Java");
		System.out.println(lib.bookCount());
		System.out.println(lib.getBorrowers());
		
	}
		

}
