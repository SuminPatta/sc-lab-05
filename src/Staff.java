import java.util.ArrayList;

public class Staff {
	
	private String name;
	private String lastname;
	private ArrayList<String> book = new ArrayList<String>();
	public Staff(String name, String lastname) {
		this.name = name;
		this.lastname = lastname;
	}
	
	public String getBorrowedBook() {
		String nameBook = "";
		for(int i = 0; i < book.size();i++){
			nameBook += book.get(i)+" ";
		}
		return nameBook;
	}

	public String getName() {
		return (name+" "+lastname);
	}
	public void setBorrowedBook(String nameBook) {
		book.add(nameBook);
	}
	public void setReturnedBook(String nameBook) {
		book.remove(nameBook);
	}


}
