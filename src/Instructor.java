import java.util.ArrayList;

public class Instructor {
	private String name;
	private String lastname;
	private String id;
	private ArrayList<String> book = new ArrayList<String>();
	
	public Instructor(String name, String lastname, String id) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.lastname = lastname;
		this.id = id;
	}
	public String getBorrowedBook() {
		// TODO Auto-generated method stub
		String nameBook = "";
		for(int i = 0; i < book.size();i++){
			nameBook += book.get(i)+" ";
		}
		return nameBook;
	}

	public String getName() {
		// TODO Auto-generated method stub
		
		return (name+" "+lastname);
	}
	public void setBorrowedBook(String nameBook) {
		book.add(nameBook);
	}
	public void setReturnedBook(String nameBook) {
		book.remove(nameBook);
	}

}
