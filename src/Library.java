import java.util.ArrayList;

public class Library {
	private ArrayList<Book> b = new ArrayList<Book>();
	private ArrayList<ReferencesBook> rb = new ArrayList<ReferencesBook>();
	private ArrayList<Book> bb = new ArrayList<Book>();
	private ArrayList<String> borrower = new ArrayList<String>();
	static private int count;
	
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		b.add(book);
		
	}

	public void addBook(ReferencesBook refbook) {
		// TODO Auto-generated method stub
		rb.add(refbook);
		
	}

	public int bookCount() {
		// TODO Auto-generated method stub
		count = b.size()+rb.size();
		return count;
	}

	public void borrowBook(Student stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < b.size(); i++){
			if (b.get(i).getBookName().equals(nameBook)){
				bb.add(b.get(i));
				b.remove(i);
				borrower.add(stu.getName());
				stu.setBorrowedBook(nameBook);
			}
		}
	}
	public void borrowBook(Instructor stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < b.size(); i++){
			if (b.get(i).getBookName().equals(nameBook)){
				bb.add(b.get(i));
				b.remove(i);
				borrower.add(stu.getName());
				stu.setBorrowedBook(nameBook);
			}
		}
	}
	public void borrowBook(Staff stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < b.size(); i++){
			if (b.get(i).getBookName().equals(nameBook)){
				bb.add(b.get(i));
				b.remove(i);
				borrower.add(stu.getName());
				stu.setBorrowedBook(nameBook);
			}
		}
	}

	public String getBorrowers() {
		// TODO Auto-generated method stub
		String bow = "";
		for(int i =0; i <borrower.size();i++){
			bow += borrower.get(i)+ " ";
		}
		return bow;
	}

	public void returnBook(Student stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < bb.size(); i++){
			if (bb.get(i).getBookName().equals(nameBook)){
				b.add(bb.get(i));
				bb.remove(i);
				borrower.remove(stu.getName());
				stu.setReturnedBook(nameBook);
			}
		}
	}
	
	public void returnBook(Instructor stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < bb.size(); i++){
			if (bb.get(i).getBookName().equals(nameBook)){
				b.add(bb.get(i));
				bb.remove(i);
				borrower.remove(stu.getName());
				stu.setReturnedBook(nameBook);
			}
		}
	}
	
	public void returnBook(Staff stu, String nameBook) {
		// TODO Auto-generated method stub
		for(int i = 0; i < bb.size(); i++){
			if (bb.get(i).getBookName().equals(nameBook)){
				b.add(bb.get(i));
				bb.remove(i);
				borrower.remove(stu.getName());
				stu.setReturnedBook(nameBook);
			}
		}
	}

}
